<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Distrito extends Model
{
    use HasFactory;

    protected $table = 'distritos';
    protected $primaryKey = 'cod_dis';
    public $incrementing = false;
    protected $keyType = 'char';
    protected $fillable = ['cod_dis', 'nom_dis'];

    public function vendedores()
    {
        return $this->hasMany(Vendedor::class, 'cod_dis', 'cod_dis');
    }

    public function clientes()
    {
        return $this->hasMany(Cliente::class, 'cod_dis', 'cod_dis');
    }

    public function proveedores()
    {
        return $this->hasMany(Proveedor::class, 'cod_dis', 'cod_dis');
    }
}

