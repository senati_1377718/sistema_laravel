<!-- resources/views/proveedores/index.blade.php -->

<x-slot name="header">
    <x-title-list icon="users">{{ __('Proveedores') }}</x-title-list>
</x-slot>

<div class="max-w-7xl py-6 mx-auto sm:px-4 lg:px-6 space-y-6">
    <div class="p-4 sm:p-8 bg-white dark:bg-secondary-800 shadow sm:rounded-lg">
        <x-session-status />
        <div class="flex flex-col">
            <div class="inline-block min-w-full">
                <x-primary-button type="button" wire:click="create()" class="mb-2">
                    <i class="fa-solid fa-plus me-1"></i>{{ __('Crear Proveedor') }}
                </x-primary-button>
                <div class="rounded overflow-x-auto">
                    <table class="min-w-full text-left text-sm font-light">
                        <thead class="border-b bg-secondary-800 font-medium text-white dark:border-secondary-500 dark:bg-secondary-900">
                        <tr>
                            <x-table-th title="{{ __('Razón Social') }}" />
                            <x-table-th title="{{ __('Dirección') }}" />
                            <x-table-th title="{{ __('Teléfono') }}" />
                            <x-table-th title="{{ __('Representante') }}" />
                            <th scope="col" class="px-6 py-4">{{ __('Acciones') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse ($proveedores as $proveedor)
                            <tr class="border-b transition duration-300 ease-in-out hover:bg-secondary-100 dark:border-secondary-500 dark:hover:bg-secondary-600">
                                <x-table-td>{{ $proveedor->RSO_PRV }}</x-table-td>
                                <x-table-td>{{ $proveedor->DIR_PRV }}</x-table-td>
                                <x-table-td>{{ $proveedor->TEL_PRV }}</x-table-td>
                                <x-table-td>{{ $proveedor->REP_PRV }}</x-table-td>
                                <x-table-td>
                                    <x-table-buttons id="{{ $proveedor->COD_PRV }}" />
                                </x-table-td>
                            </tr>
                        @empty
                            <x-table-empty colspan="5" />
                        @endforelse
                        </tbody>
                    </table>
                </div>
                <div class="mt-2">
                    {{ $proveedores->links() }}
                </div>
            </div>
        </div>

        <!-- Incluir modales -->
        @if ($agregarProveedor)
            @include('proveedores.create')
        @endif

        @if ($actualizarProveedor)
            @include('proveedores.edit')
        @endif

        @if ($eliminarProveedor)
            <x-table-modal-delete model="eliminarProveedor" />
        @endif
    </div>
</div>
