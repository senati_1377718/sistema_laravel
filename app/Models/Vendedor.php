<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Vendedor extends Model
{
    use HasFactory;

    protected $table = 'vendedores';
    protected $primaryKey = 'cod_ven';
    public $incrementing = false;
    protected $keyType = 'char';
    protected $fillable = ['cod_ven', 'nom_ven', 'ape_ven', 'sue_ven', 'fti_ven', 'tip_ven', 'cod_dis'];

    public function distrito()
    {
        return $this->belongsTo(Distrito::class, 'cod_dis', 'cod_dis');
    }

    public function facturas()
    {
        return $this->hasMany(Factura::class, 'cod_ven', 'cod_ven');
    }
}
