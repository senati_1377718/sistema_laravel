<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrdenCompra extends Model
{
    use HasFactory;

    protected $table = 'ordenes_compra';
    protected $primaryKey = 'num_oco';
    public $incrementing = false;
    protected $keyType = 'char';
    protected $fillable = ['num_oco', 'fec_oco', 'cod_prv', 'fat_oco', 'est_oco'];

    public function proveedor()
    {
        return $this->belongsTo(Proveedor::class, 'cod_prv', 'cod_prv');
    }

    public function detalleCompras()
    {
        return $this->hasMany(DetalleCompra::class, 'num_oco', 'num_oco');
    }
}
