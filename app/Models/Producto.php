<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
    use HasFactory;

    protected $table = 'productos';
    protected $primaryKey = 'cod_pro';
    public $incrementing = false;
    protected $keyType = 'char';
    protected $fillable = ['cod_pro', 'des_pro', 'pre_pro', 'sac_pro', 'smi_pro', 'uni_pro', 'lin_pro', 'imp_pro'];

    public function detalleFacturas()
    {
        return $this->hasMany(DetalleFactura::class, 'cod_pro', 'cod_pro');
    }

    public function detalleCompras()
    {
        return $this->hasMany(DetalleCompra::class, 'cod_pro', 'cod_pro');
    }

    public function abastecimientos()
    {
        return $this->hasMany(Abastecimiento::class, 'cod_pro', 'cod_pro');
    }
}
